#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'


function network_owner() {
  local ip=$(dig ${1} +short|tail -1)
  if [[ -z $ip ]]; then
    echo "DOES_NOT_RESOLVE"
  else
    local who_is=$(whois ${ip}|grep OrgName|sed -E 's/^.*: +//')
    if [[ -z $who_is ]]; then
      echo "N/A"
    else
      echo "$who_is"
    fi
  fi
}

function rev_name() {
  local ip=$(dig ${1} +short|tail -1)
  if [[ -z $ip ]]; then
    echo "DOES_NOT_RESOLVE"
  else
    local rev_ip=$(dig -x ${ip} +short)
    if [[ -z $rev_ip ]]; then
      echo $ip
    else
      echo $rev_ip
    fi
  fi
}

function port_open() {
  if (nc -z -w5 -G1 ${1} ${2} >/dev/null 2>&1); then
    echo Yes
  else
    echo No
  fi
}

function http_status() {
  local status=$((curl --head --connect-timeout 5 --max-time 5 --silent ${1}|head -1|cut -d\  -f2) || echo "Error")
  if [[ -z $status ]]; then
    echo "Invalid"
  else
    echo "$status"
  fi
}


function redirect() {
  if ! curl --head --connect-timeout 5 --max-time 5 --silent "$1"|grep Location|sed -E 's/^.*: +//'|cut -c 1-40; then
    echo "-"
  fi
}

function run() {
  printf '%s\t%s\t%s\t%s\t%s\t%s\n' "HOST" "NETWORK" "REV" "HTTPS" "SSH" "REDIRECT"
  for host in "$@"
  do
    printf "%s\t%s\t%s\t%s\t%s\t%s\n" "$host" $(network_owner $host) $(rev_name $host) $(http_status "https://$host") $(port_open $host 22) $(redirect "https://$host")
  done
}

run $@ | column -t -s $'\t'
