# Resolve repo & wiki failures

## First and foremost

*Don't Panic*

## Diagnose errors

- Visit [Kibana](https://log.gitlab.net/app/kibana)
- Select `pubsub-geo-inf-gprd`
- Search for `Error syncing repository`

### Export wiki failures to CSV

```ruby
failures = Geo::ProjectRegistry.failed_wikis
data = CSV.open('/tmp/failures.csv', 'w')
failures.each do |fail|
   data << [fail.project_id, fail.last_wiki_sync_failure&.gsub("\n", " ")]
end
data.close
```

### Get count of unsynced wikis

```ruby
finder = Geo::ProjectRegistryFinder.new
synced_wikis = finder.send(:fdw_find_synced_wikis).pluck(:project_id); nil
wikis = Project.with_wiki_enabled.pluck(:id); nil
diff = wikis - synced_wikis
diff.count
```

Extract the failures:

```ruby
finder.count_failed_wikis

Geo::ProjectRegistry.where(project_id: diff).where('wiki_retry_count > 0').count
```

## Force wiki resync

When you have a list of wikis you'd like to resync:

```ruby
wiki_registries_to_resync.update_all(wiki_retry_count: true)
```

## TODO

Complete with steps taken from:

 - gitlab-com/migration#409
 - gitlab-com/migration#365
